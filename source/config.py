
AWS_CONN_ID = "<aws-connection-id>"
AWS_BUCKET_ID = "<add-s3-bucket-id>"
AWS_S3_KEY_PREFIX = "<add-s3-key-prefix>"

BETFAIR_USERNAME = "<username>"
BETFAIR_PASSWORD = "<password>"
BETFAIR_APP_KEY = "<appkey>"

POSTGRES_CONNECTION_ID = "<postgres-connection>"
