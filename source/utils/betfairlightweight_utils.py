from typing import Union

from betfairlightweight import APIClient
from betfairlightweight.endpoints.keepalive import KeepAlive
from betfairlightweight.resources.authresources import KeepAliveResource

from betfair_broker_pipeline.source.config import BETFAIR_USERNAME, BETFAIR_PASSWORD, BETFAIR_APP_KEY
from betfair_broker_pipeline.source.models.bet_record import BetRecord


def get_logged_client() -> APIClient:
    client = APIClient(BETFAIR_USERNAME, BETFAIR_PASSWORD, app_key=BETFAIR_APP_KEY)
    client.login_interactive()
    return client

def request_keep_alive(client: APIClient) -> Union[dict, KeepAliveResource]:
    keep_alive = KeepAlive(client)
    return keep_alive()
