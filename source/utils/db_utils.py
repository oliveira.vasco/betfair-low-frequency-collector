from decimal import Decimal
from contextlib import contextmanager
from typing import Dict, List, Set, Union

from airflow.providers.postgres.hooks.postgres import PostgresHook

from betfair_low_frequency_collector.source.config import POSTGRES_CONNECTION_ID

SELECT_ENABLED_MARKETS = "SELECT * FROM collection_market WHERE enabled = TRUE;"
INSERT_COLLECTION_LOG = "INSERT INTO collection_log (market_id, file_path) VALUES (%s, %s) RETURNING id;"


@contextmanager
def postgres_cursor(with_commit: bool = False):
    postgres_hook = PostgresHook(postgres_conn_id=POSTGRES_CONNECTION_ID)
    connection = postgres_hook.get_conn()
    cursor = connection.cursor()
    try:
        yield cursor
        if with_commit:
            connection.commit()
    finally:
        cursor.close()
        connection.close()


def query_enabled_markets() -> Set[str]:
    with postgres_cursor() as cursor:
        cursor.execute(SELECT_ENABLED_MARKETS)
        collection_markets = cursor.fetchall()
    return list(map(lambda m: m[0], collection_markets))


def insert_collection_log(market_id: str, file_path: str) -> Dict:
    with postgres_cursor(with_commit=True) as cursor:
        cursor.execute(INSERT_COLLECTION_LOG, (market_id, file_path),)
        row_id = cursor.fetchone()[0]
    return row_id
