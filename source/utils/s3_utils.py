import json
import requests
from datetime import date, datetime
from typing import List

from airflow.providers.amazon.aws.hooks.s3 import S3Hook   

from betfair_low_frequency_collector.source.config import AWS_BUCKET_ID, AWS_CONN_ID, AWS_S3_KEY_PREFIX


def upload_dict_to_s3(content_type: str, content: dict) -> str:
    s3_key = f"{AWS_S3_KEY_PREFIX}/{content_type}-{datetime.timestamp(datetime.now())}.json"
    s3_hook = S3Hook(aws_conn_id=AWS_CONN_ID)
    s3_hook.load_string(string_data=json.dumps(content), key=s3_key, bucket_name=AWS_BUCKET_ID)
    return s3_key
