from typing import List

from airflow.operators.postgres_operator import PostgresOperator

from betfair_low_frequency_collector.source.config import POSTGRES_CONNECTION_ID


def initialise_db_tables() -> PostgresOperator:
    return PostgresOperator(
        task_id="initialise_db_tables",
        sql = "sql/create_collection_market_table.sql",
        postgres_conn_id = POSTGRES_CONNECTION_ID,
    )
