from airflow.decorators import task_group

from betfair_low_frequency_collector.source.tasks.query_markets_to_collect import query_markets_to_collect
from betfair_low_frequency_collector.source.tasks.fetch_market_data import fetch_market_data


@task_group(group_id="collection_task_group")
def collection_task_group():
    market_ids = query_markets_to_collect()
    fetch_market_data(market_ids)
    return market_ids
