from airflow.decorators import task

from betfair_low_frequency_collector.source.utils.db_utils import query_enabled_markets


@task
def query_markets_to_collect():
    return query_enabled_markets()
