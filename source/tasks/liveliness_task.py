from airflow.decorators import task

from betfair_low_frequency_collector.source.constants import RESPONSE_SUCCESS
from betfair_low_frequency_collector.source.utils.betfairlightweight_utils import get_logged_client, request_keep_alive


@task
def check_liveliness() -> None:
    api_client = get_logged_client()
    keep_alive = api_client.keep_alive()
    assert RESPONSE_SUCCESS == keep_alive.status
    return None
