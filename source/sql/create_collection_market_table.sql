CREATE TABLE IF NOT EXISTS collection_market (
    market_id VARCHAR(20) PRIMARY KEY,
    name VARCHAR,
    enabled BOOLEAN NOT NULL DEFAULT TRUE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);


CREATE TABLE IF NOT EXISTS collection_log (
    id SERIAL PRIMARY KEY,
    market_id VARCHAR(20) NOT NULL,
    file_path varchar NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    CONSTRAINT fk_market_id FOREIGN KEY (market_id) REFERENCES collection_market(market_id)
);

CREATE INDEX IF NOT EXISTS index_market_id ON collection_log (market_id);
