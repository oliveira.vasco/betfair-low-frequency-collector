from airflow.decorators import dag

from datetime import datetime

from betfair_low_frequency_collector.source.tasks.initialise_db_tables_task import initialise_db_tables
from betfair_low_frequency_collector.source.tasks.liveliness_task import check_liveliness
from betfair_low_frequency_collector.source.tasks.collection_task_group import collection_task_group

default_args = {
    "start_date": datetime(2022, 1, 1)
}


@dag(schedule_interval="15 */3 * * *", default_args=default_args, catchup=False)
def betfair_low_frequency_collector():
    initialise_db_tables() >> check_liveliness() >> collection_task_group()

dag = betfair_low_frequency_collector()
